export default {
  pages: {
    global: {
      filterTooltipPrefix: "Compatibilidade:",
      taxIncluded: "Todos os preços incluem IVA a {taxRate}%"
    },
    menu: {
      options: {
        home: "Início",
        catalog: "Catálogo",
        cart: "Carrinho de Compras",
        basket: "Encomenda",
        checkout: "Dados Entrega",
        resume: "Resumo",
        confirmation: "Confirmação",
        product: "Detalhe",
        login: "Entrar",
        recover: "Recuperar acesso",
        logout: "Saír",
        register: "Registar",
        activate: "Activar",
        account: "A minha conta",
        activateAccount: "Activar a minha conta de acesso",
        resetAccount: "Recuperar conta",
        info: {
          label: "Informação",
          contacts: "Contactos",
          location: "Localização",
          salesRules: "Condições de venda",
          legal: "Resolução de Litígios Online",
          privacy: "Política de Proteção de Dados",
          cookies: "Política de Cookies"
        }
      }
    },
    home: {
      myBikeLabel: "A MINHA MOTO",
      noBikeLabel: "Selecionar a minha moto...",
      selectMyBikeLabel: "Escreva a marca ou modelo da sua moto...",
      filterCategories: "Filtrar categorias..."
    },
    login: {
      header: "Login",
      subheader: "Introduza o seu email e palavra-passe"
    },
    account: {
      register: {
        header: "Criar conta de acesso",
        subheader: "Por favor, preencha o formulário com os seus dados",
        checkYourEmail:
          "Verique o seu Email: foi enviado um link de activação (válido por 48h)",
        emailExists: "Endereço de Email já registado",
        invalidFields:
          "Os campos assinalados a vermelho apresentam valores inválidos. Por favor, corrigir e tentar de novo"
      },
      activation: {
        header: "Activar conta",
        subheader: "Por favor, defina a sua senha de acesso",
        activated: "Conta está agora activa",
        activatedLogin: "Identificar-se aqui",
        activatedRecover: "ou recuperar senha de acesso...",
        notFound: "Conta inexistente",
        forbiden: "Link inválido ou conta já se encontra activa",
        error: "Erro ao activar conta, por favor tente de novo",
        hashNotFoundOrInvalid:
          "Código de activação, email ou palavra-passe inválidos",
        passwordLabel: "palavra-passe *",
        confirmPasswordLabel: "Confirmar palavra-passe *",
        passwordHintLabel: "* Minimo: 8 caracteres",
        passwordShowLabel: "Mostrar palavra-passe",
        passwordHideLabel: "Esconder palavra-passe"
      },
      edit: {
        header: "Modificar dados da minha conta de acesso",
        loadError: "Erro ao obter dados da conta. Tente mais tarde, por favor.",
        logout: "Saír",
        success: "Alterações guardadas com sucesso",
        fail: "Erro ao guardar alterações! Tente de novo, por favor.",
        invalidFields:
          "Os campos assinalados a vermelho apresentam valores inválidos. Por favor, corrigir e tentar de novo"
      },
      recover: {
        header: "Recuperar acesso",
        subheader:
          "Introduza o endereço de email com o qual se registou inicialmente",
        submitLabel: "Recuperar agora",
        emailSent: "Email enviado, verifique a sua caixa de correio.",
        sucessful: "Conta recuperada com sucesso! Pode efectuar Login."
      }
    },
    catalog: {
      header: "Catálogo",
      subheader: "Todos os produtos",
      pagination: {
        page: "Página",
        previous: "Anterior",
        next: "Próxima",
        of: " de "
      },
      item: {
        refLabel: "Ref:",
        addToCartLabel: "Comprar",
        detailLabel: "Consultar..."
      },
      compatible: {
        with: "Compatível com",
        absent: "Não foi possível confirmar a compatibilidade",
        absentTip:
          "Por favor, selecione a sua moto para visualizar apenas artigos compatíveis.",
        warn:
          "Por favor, contacte-nos para confirmar compatibilidade com a sua moto.",
        confirmNonCompatibleOrder:
          "Tem a certeza que quer efectuar encomenda sem confirmar compatibilidade?"
      },
      error: {
        noDataFoundBike: {
          label: "Upsss!",
          notice:
            "De momento não dispomos de artigos compatíveis com {model} do ano {year}.",
          noticeWithFilter:
            'De momento não dispomos de artigos correspondentes à palavra-chave "{keyword}", compatíveis com {model} do ano {year}.',
          noticeWithFilter2:
            'Experimente remover a palavra-chave "{keyword}" da pesquisa, ou em alternativa:',
          link: "Por favor, contacte-nos para obter o artigo que procura."
        },
        noDataFound: {
          label: "Upsss!",
          notice:
            "Não foram encontrados artigos nesta categoria ou combinada com a pesquisa.",
          noticeWithFilter:
            'De momento não dispomos de artigos correspondentes à palavra-chave "{keyword}".',
          noticeWithFilter2:
            'Experimente remover a palavra-chave "{keyword}" da pesquisa, ou em alternativa:',
          link: "Por favor, contacte-nos para obter o artigo que procura."
        }
      }
    },
    product: {
      header: "Producto",
      subheader: "Detalhe do producto",
      item: {
        refLabel: "Ref:",
        addToCartLabel: "Comprar",
        detailLabel: "Consultar..."
      }
    },
    cart: {
      header: "Carrinho de Compras",
      subheader: "Artigos na sua encomenda",
      resumeHeader: "Carrinho de Compras - Resumo",
      resumeSubheader: "Finalize a sua compra",
      add: {
        success: "Artigo adicionado",
        error: "Artigo não adicionado, por favor, tente de novo"
      },
      totals: {
        productsLabel: "ARTIGOS",
        taxesLabel: "IVA",
        shippingMethodLabel: "MÉTODO PAGAMENTO",
        shippingCostLabelTRF: "Transferência",
        shippingCostLabelCTE: "Cobrança",
        shippingCostNotice:
          "O custo estimado de expedição varia mediante a opção selecionada.",
        shippingCost: "TAXAS EXPEDIÇÃO",
        shippingCostUnknow: "SOB CONSULTA",
        shippingLabel: "ACRESCE VALOR PORTES A CONFIRMAR SOB/CONSULTA",
        grandTotal: "TOTAL",
        free: "GRÁTIS"
      },
      shippingHint:
        "title: Peso total superior a 5 Kgs. O custo final será apurado e comunicado por telefone ou email. Encomenda será validada apenas com acordo do Cliente.",
      resetMessage: "Remover todo os artigos do carrinho?",
      checkoutLabel: "Concluír compra",
      resetLabel: "Remover todos",
      cartIsEmpty: "O carrinho de compras está vazio!",
      continueLabel: "Continuar a comprar",
      viewFullCart: "Ver carrinho de compras completo...",
      confirmSubmitWithNoModel:
        "Não foi possível indentificar o modelo da sua moto.<br />Não podemos assegurar a compatibilidade dos artigos na encomenda.<br /><br /><strong>Pretende continuar?</strong><br /><br /><i>Por favor, contacte-nos para que possamos prestar ajuda.</i>"
    },
    checkout: {
      header: "Informação Pessoal",
      subheader:
        "Confirme os seus dados, morada de entrega e método de pagamento",
      hintLabel: "Sugestão:",
      hintText: "Para evitar introduzir manualmente os seguintes dados, ",
      login: "identifique-se",
      loginOr: " ou crie uma ",
      register: "conta de acesso",
      resume: "Ver resumo & confirmar",
      placeOrder: "Colocar a minha encomenda agora"
    },
    resume: {
      header: "Resumo & Confirmação",
      subheader:
        "Confirme os artigos, valores e restantes dados e coloque a encomenda em seguida",
      products: "Artigos",
      delivery: "Dados de Entrega & Pagamento",
      hintLabel: "Sugestão:",
      hintText: "Para evitar introduzir manualmente os seguintes dados, ",
      login: "identifique-se",
      loginOr: " ou crie uma ",
      register: "conta de acesso",
      placeOrder: "Colocar a minha encomenda agora",
      placeOrderHint:
        "Não fará qualquer pagamento agora. Será contactado pela loja para confirmação. Obrigado!",
      orderPlaced:
        "Encomenda colocada com sucesso! Será contactado em breve, obrigado pela preferência."
    }
  },
  widgets: {
    bikeSelector: {
      modalTitle: "A MINHA MOTO",
      selectorLabel: "Escreva a marca ou modelo da sua moto...",
      selectorHint: "Demasiadas opções, introduza mais detalhe (modelo/versão)",
      invalidBikeWarn:
        "Não existem produtos no Catálogo para este modelo. Por favor, contacte-nos.",
      selectYear: "Selecione o ano:",
      selectedYear: "Ano selecionado:",
      allYears: "Qualquer ano"
    }
  },
  name: "Bike Fit",
  register: "Registar",
  logout: "Saír",
  settings: "Definições",
  test: "Teste",
  auth: {
    login: {
      authorized: "Autenticado com sucesso",
      unauthorized: "Email or palavra-passe incorrectos",
      maxFailedAttempts:
        "Esgotou tentativas com email ou password errados. Por favor, aguarde 30 mins a tente de novo",
      unhandled: "Erro desconhecido",
      server: "Erro de servidor"
    }
  },
  actions: {
    ok: "Ok",
    cancel: "Cancelar",
    confirm: "Confirmar",
    back: "Voltar",
    forward: "Avançar",
    open: "Open",
    close: "Close",
    pick: "Pick",
    select: "Select",
    submit: "Submit",
    reset: "Reset",
    add: "Add",
    delete: "Delete",
    remove: "Remove",
    swipe: "Swipe",
    left: "Left",
    right: "Right"
  },
  language: "Linguagem",
  back: "Voltar",
  news_feed: "Feed Notícias",
  close: "Fechar",
  ok: "OK",
  open_popup: "Abrir janela",
  connection_error: "Erro de comunicações",
  error: {
    title: "Erro",
    server: "Erro de Servidor",
    general: "Erro genérico",
    connection: "Erro de comunicações",
    database: "Erro de base de dados",
    unknown: "Erro desconhecido",
    keyNotFound: "Item não encontrado na lista"
  },
  email: "Email",
  password: "Password",
  register_new_account: "Registar nova conta!",
  first_name: "Nome",
  last_name: "Sobrenome",
  confirm_password: "Confirmar Password",
  have_an_account: "Já tens conta? Aceder",
  login_success: "Fez Login com sucesso",
  register_success: "Fez Login com sucesso",
  logout_success: "Fez Logout com sucesso",
  left_menu: "Opções",
  popup: "Popup",
  cancel: "Cancelar",
  offline_message: "Está offline",
  online_message: "Está online",
  please_wait: "por favor, aguarde...",
  messages: {
    _default: function(e) {
      return "The " + e + " value is not valid.";
    },
    after: function(e, n) {
      var t = n[0];
      return "The " + e + " must be after " + t + ".";
    }
  }
};
