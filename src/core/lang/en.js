export default {
  pages: {
    global: {
      filterTooltipPrefix: "Products for:"
    },
    menu: {
      options: {
        home: "Home",
        catalog: "Catalog",
        cart: "Cart",
        basket: "Basket",
        checkout: "Personal Information",
        confirmation: "Confirmation",
        resume: "Resume",
        product: "Detail",
        login: "Login",
        logout: "Logout",
        register: "Register",
        activate: "Activate",
        account: "Account"
      }
    },
    home: {
      myBikeLabel: "MY BIKE",
      noBikeLabel: "Select my bike...",
      selectMyBikeLabel: "Input your bike brand or model...",
      filterCategories: "Filter categories..."
    },
    login: {
      header: "Please, login",
      subheader: "Please, enter your email and password"
    },
    account: {
      register: {
        header: "Create new account",
        subheader: "Please, provided your info & preferred options",
        checkYourEmail:
          "Check your Inbox: an activation link has been sent (valid for 48 hours)",
        emailExists: "Email already registered",
        invalidFields:
          "The fields marked in red have wrong values, please revise and try again"
      },
      activation: {
        header: "Activate your account",
        subheader: "Please, choose a password",
        activated: "Account activated",
        activatedLogin: "Login here",
        activatedRecover: "or recover your password...",
        notFound: "Account not found",
        forbiden: "Account already activated",
        error: "Error activating account, please try again",
        hashNotFoundOrInvalid: "Activation code, email or password missing",
        passwordLabel: "Password *",
        confirmPasswordLabel: "Confirm password *",
        passwordHintLabel: "* Minimum 8 chars"
      }
    },
    catalog: {
      header: "Catalog",
      subheader: "All products we have available",
      pagination: {
        page: "Page",
        previous: "Previous",
        next: "Next",
        of: " of "
      },
      item: {
        refLabel: "Ref:",
        addToCartLabel: "Add to cart",
        detailLabel: "Detail..."
      },
      compatible: {
        with: "For model",
        absent: "Could not ensure compatibility",
        absentTip: "Please select your bike to view compatible items.",
        warn:
          "Please, contact us to confirm compatibility with your motorcycle.",
        confirmNonCompatibleOrder:
          "Are you sure you want to place an order without confirming compatibility?"
      },
      error: {
        noDataFound: {
          label: "Upsss!",
          notice:
            "Currently, we don't have any compatible products with {model} of year {year}.",
          link: "Please, contact us to get help with your query."
        }
      }
    },
    cart: {
      header: "Shopping Cart",
      subheader: "Products in your order",
      resumeHeader: "Shopping Cart Resume",
      resumeSubheader: "Please, proceed to checkout",
      add: {
        success: "Your product was added to Cart",
        error: "Product not added to Cart, please try again"
      },
      totals: {
        productsLabel: "TOTAL (NO TAXES)",
        taxesLabel: "IVA",
        shippingLabel: "SHIPPING",
        grandTotal: "TOTAL",
        free: "FREE"
      },
      shippingHint:
        "title: Estimated costs based on current shipping handling fees. Final value to be communicated by phone or email.",
      resetMessage: "Reset Cart and clear all items?",
      checkoutLabel: "Checkout",
      resetLabel: "Clear all",
      cartIsEmpty: "Your cart is empty!",
      continueLabel: "Continue shopping",
      viewFullCart: "View cart..."
    },
    checkout: {
      header: "Personal Information",
      subheader: "Confirm you contact, shipping address and payment method",
      login: "Login",
      loginOr: " or ",
      register: "register now",
      loginOrEnd: " to recover or save your address",
      resume: "Proceed to Resume & Confirmation",
      placeOrder: "Place my order now!"
    }
  },
  widgets: {
    bikeSelector: {
      modalTitle: "MY BIKE",
      selectorLabel: "Input your bike brand or model...",
      selectorHint: "Too many options, add more detail (model/version)",
      invalidBikeWarn:
        "We couldn't find any products for this Bike. Please, contact us. ",
      selectYear: "Select make year:",
      selectedYear: "Make year selected:",
      allYears: "Any year"
    }
  },
  name: "Bike Fit",
  register: "Register",
  logout: "Logout",
  settings: "Settings",
  test: "Test",
  auth: {
    error: {
      authorized: "You are now logged in",
      unauthorized: "Wrong email or password",
      unhandled: "Unknown error",
      server: "Server error"
    }
  },
  actions: {
    ok: "Ok",
    cancel: "Cancel",
    confirm: "Confirm",
    back: "Back",
    forward: "Forward",
    open: "Open",
    close: "Close",
    pick: "Pick",
    select: "Select",
    submit: "Submit",
    reset: "Reset",
    add: "Add",
    delete: "Delete",
    remove: "Remove",
    swipe: "Swipe",
    left: "Left",
    right: "Right"
  },
  language: "Prefered Language",
  back: "Back",
  news_feed: "News Feed",
  close: "Close",
  ok: "OK",
  open_popup: "Open popup",
  connection_error: "Connection Error",
  error: {
    title: "Error",
    server: "Server error",
    general: "General error",
    connection: "Connection Error",
    database: "Database error",
    unknown: "Unknown error",
    keyNotFound: "Key not found"
  },
  email: "Email",
  password: "Password",
  register_new_account: "Register a new account !",
  first_name: "First Name",
  last_name: "Last Name",
  confirm_password: "Confirm Password",
  have_an_account: "Have an account ? Sign In",
  login_success: "You are logged in successfully",
  register_success: "You are logged up successfully",
  logout_success: "You are logged out successfully",
  left_menu: "Left Menu",
  popup: "Popup",
  cancel: "Cancel",
  offline_message: "You are offline",
  online_message: "You are online",
  please_wait: "please wait ...",
  messages: {
    _default: function(e) {
      return "The " + e + " value is not valid.";
    },
    after: function(e, n) {
      var t = n[0];
      return "The " + e + " must be after " + t + ".";
    }
  }
};
