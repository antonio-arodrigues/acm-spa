import Vue from "vue";
import VueRouter from "vue-router";

const Home = () => import("./views/Home.vue");
const Login = () => import("./views/account/Login.vue");
const Account = () => import("./views/account/Account.vue");
const AccountAdd = () => import("./views/account/AccountAdd.vue");
const AccountRecover = () => import("./views/account/Recover.vue");
const AccountReset = () => import("./views/account/Reset.vue");
const AccountActivate = () => import("./views/account/AccountActivate.vue");
const Catalog = () => import("./views/catalog/Catalog.vue");
const Product = () => import("./views/catalog/Product.vue");
const Cart = () => import("./views/cart/Cart.vue");
const Checkout = () => import("./views/cart/Checkout.vue");
const Resume = () => import("./views/cart/Resume.vue");
const Contacts = () => import("./views/info/Contacts.vue");
const Location = () => import("./views/info/Location.vue");
const SalesRules = () => import("./views/info/SalesRules.vue");
const Legal = () => import("./views/info/Legal.vue");
const Privacy = () => import("./views/info/Privacy.vue");
const Cookies = () => import("./views/info/Cookies.vue");
const Test = () => import("./views/Test.vue");
const NotFound = () => import("./views/NotFound.vue");

import { getItem } from "./utils/localStorage";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: __dirname,
  scrollBehavior(_to, _from, _savedPosition) {
    // return { x: 0, y: 0 };
    window.scrollTo({ top: 0, behavior: "smooth" });
  },
  routes: [
    {
      path: "/",
      // name: "home",
      component: Home
      // props: true
    },
    {
      path: "/login",
      name: "login",
      component: Login
      // props: true
    },
    {
      path: "/account",
      // name: "account",
      component: Account,
      // props: true,
      // set nav guard on the route definition object:
      beforeEnter: (to, _from, next) => {
        const loggedIn = getItem("token");
        if (!loggedIn && to.name !== "login") {
          next({ path: "/login" });
        } else {
          next();
        }
      }
    },
    {
      path: "/account/add/",
      // name: "accountAdd",
      component: AccountAdd
      // props: true
    },
    {
      path: "/account/activate/:hash",
      // name: "accountActivate",
      component: AccountActivate
      // props: true
    },
    {
      path: "/account/recover/",
      // name: "accountRecover",
      component: AccountRecover
      // props: true
    },
    {
      path: "/account/reset/:hash",
      // name: "accountReset",
      component: AccountReset
      // props: true
    },
    {
      path: "/account/edit/",
      name: "accountEdit",
      component: AccountAdd,
      // props: true,
      // set nav guard on the route definition object:
      beforeEnter: (to, _from, next) => {
        const loggedIn = getItem("token");
        if (!loggedIn && to.name !== "login") {
          next({ path: "/login" });
        } else {
          next();
        }
      }
    },
    {
      path: "/catalog",
      // name: "catalog",
      component: Catalog
    },
    {
      path: "/catalog/brand/:id",
      // name: "catalogByBrand",
      component: Catalog
      // props: true
    },
    {
      path: "/product/:id",
      // name: "product",
      component: Product
      // props: true
    },
    {
      path: "/cart",
      // name: "cart",
      component: Cart
      // props: true
    },
    {
      path: "/checkout",
      // name: "checkout",
      component: Checkout
      // props: true
    },
    {
      path: "/checkout/resume",
      // name: "checkoutResume",
      component: Resume
      // props: true
    },
    {
      path: "/contacts",
      // name: "contacts",
      component: Contacts
      // props: true
    },
    {
      path: "/location",
      // name: "location",
      component: Location
      // props: true
    },
    {
      path: "/sales-rules",
      // name: "salesRules",
      component: SalesRules
      // props: true
    },
    {
      path: "/legal",
      // name: "legal",
      component: Legal
      // props: true
    },
    {
      path: "/privacy",
      // name: "privacy",
      component: Privacy
      // props: true
    },
    {
      path: "/cookies",
      // name: "cookies",
      component: Cookies
      // props: true
    },
    {
      path: "/test",
      // name: "test",
      component: Test,
      props: true
    },
    {
      path: "*",
      // name: "NotFound",
      component: NotFound,
      props: true
    }
  ]
});

export default router;
