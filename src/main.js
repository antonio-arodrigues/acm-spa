/* global UIkit */
import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import moment from "moment";
import VueMomentJS from "vue-momentjs";
import Suggestions from "v-suggestions";
import "v-suggestions/dist/v-suggestions.css"; // you can import the stylesheets also (optional)
import VeeValidate from "vee-validate";
import VueCurrencyFilter from "vue-currency-filter";
import VueCookies from "vue-cookies";

// app core
import App from "./App.vue";
import router from "./router";
import { store } from "./store/store";
import { i18n } from "./core/i18n";
import { getItem, setItem } from "./utils/localStorage";
import { unsetAccountData } from "./store/utils";

// Get ENV
const ENV = process.env.NODE_ENV || "production";
console.log("PROCESS_ENV:", ENV);

// external libs
Vue.use(VueCookies);
Vue.use(VueMomentJS, moment);
Vue.use(VueAxios, axios);
Vue.component("suggestions", Suggestions);
Vue.use(VeeValidate);
// set default config
VueCookies.config("7d");
// currency
Vue.use(VueCurrencyFilter, {
  symbol: "€",
  thousandsSeparator: ".",
  fractionCount: 2,
  fractionSeparator: ",",
  symbolPosition: "front",
  symbolSpacing: true
});

// config
Vue.conf = require("./config").items;
Vue.prototype.$config = Vue.conf;

Vue.config.productionTip = false;
Vue.config.devtools = process.env.NODE_ENV === "development";

// Vue.use(VueCurrencyFilter, Vue.config.get("global").currencyConfig);

// Notifications
const APP_NOTIFICATION_POSITION = "top-center";
const getNotificationIcon = type => {
  const mapTypeToIcon = {
    primary: "check",
    success: "cart",
    warn: "warning",
    danger: "ban",
    default: "info"
  };
  return mapTypeToIcon[type] || mapTypeToIcon["default"];
};

/**
 *  Application object
 */
Vue.app = {
  i18n,
  store,
  initialize: function() {
    /**
     * set global API url
     */
    const API = Vue.app.config.get("api")[ENV].slug;
    console.log("[API]", API);

    /**
     * append access token to every request
     */
    Vue.axios = axios.create({
      baseURL: API,
      timeout: false,
      params: {}
    });

    // Add a request interceptor
    Vue.axios.interceptors.request.use(
      config => {
        let token = getItem("token");
        if (token) {
          config.headers.common["Authorization"] = "Bearer " + token;
          VueCookies.set("token", token);
        }
        config.withCredentials = true;
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );

    Vue.axios.interceptors.response.use(
      response => {
        if (response && response.data && response.data.token) {
          // determine if we need to update jwtToken
          const token = response.data.token;
          if (getItem("token") !== token) {
            setItem("token", token);
            VueCookies.set("token", token);
            store.dispatch("setToken", token);
          }
        }
        return response;
      },
      error => {
        const currentRoute = router.app._route.name;
        if (error.response.status === 401) {
          if (currentRoute === "login") {
            return error.response;
          } else {
            UIkit.modal.alert(
              "A sua sessão expirou. Volte a introduzir as suas credenciais, por favor."
            );
            unsetAccountData();
            VueCookies.set("token", "");
            console.warn(
              "Token not found or expired, you have been logged out"
            );
            router.push("/login");
            return Promise.resolve();
          }
        }
        return Promise.reject(error);
      }
    );

    /**
     * initializing vue instance
     * @type {Vue$3|Vue}
     */
    this.vue = new Vue({
      el: "#app",
      render: h => h(App),
      i18n,
      store,
      router,
      mounted: function() {
        // On offline connection
        document.addEventListener(
          "offline",
          () => {
            console.error({
              message: Vue.app.trans("offline_message"),
              hold: false
            });
          },
          false
        );

        // On online connection
        document.addEventListener(
          "online",
          () => {
            // Don't do any thing
            // Vue.app.f7.addNotification({ message: Vue.app.trans('online_message'), hold: false })
          },
          false
        );
      }
    });
  },

  on: function(service, callback) {
    return document.addEventListener(service, () => {
      callback(Vue.app);
    });
  },

  /**
   * application config
   */
  config: {
    /**
     * get config item value
     * @param item
     * @returns {boolean}
     */
    get: function(item, options) {
      if (item && item.toLowerCase() === "formatdate" && Vue.conf["date"]) {
        const formatPlaceholder =
          store.getters.locale === "pt"
            ? Vue.conf["global"].date.formatPt
            : Vue.conf["global"].date.formatDefault;
        if (!options) return formatPlaceholder;
        return Vue.moment(options).format(formatPlaceholder);
      }
      return Vue.conf[item] !== undefined ? Vue.conf[item] : false;
    },

    /**
     * set config item value
     * @param name
     * @param value
     */
    set: function(name, value) {
      Vue.conf[name] = value;
    }
  },

  auth: {
    /**
     * get logged account
     * @returns {boolean}
     */
    account: function(field) {
      if (field) {
        return store.getters.account[field] !== undefined
          ? store.getters.account[field]
          : false;
      }

      return store.getters.account;
    },

    /**
     * get account token
     * @returns {Vue.app.auth.token|*|getters.token|null|token}
     */
    token: function() {
      return store.getters.token;
    },

    /**
     * check user
     * @returns {boolean}
     */
    check: function() {
      return store.getters.loggedIn;
    },

    logout: function() {
      Vue.app.store.commit("logout");
      setTimeout(() => {
        Vue.app.router.load("/");
      }, 1200);
    }
  },

  /**
   * get current application locale
   */
  locale: function() {
    return this.store.getters.locale;
  },

  /**
   * get translations
   * @param key
   * @returns {TranslateResult|VueI18n.TranslateResult|*}
   */
  trans: function(key) {
    return this.i18n.t(key, this.store.getters.locale);
  },

  /**
   * display a notification overlay
   * @param key
   * @param classType
   */
  notify: function(key, classType = "primary") {
    UIkit.notification({
      message: `<span uk-icon='icon: ${getNotificationIcon(
        classType
      )}'></span> ${this.trans(key)}`,
      status: classType,
      pos: APP_NOTIFICATION_POSITION,
      timeout: 3000
    });
  }
};

Vue.prototype.$app = Vue.app;

// APP init
Vue.app.initialize();

// bind Vue DevTools (Chrome)
window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = Vue.app.constructor;
