export const items = {
  api: {
    version: "1.0",
    development: {
      slug: "http://localhost:8000/api/v1/"
    },
    production: {
      slug: "https://api.acessmotos.com/v1/"
    }
  },
  global: {
    itemsPerPage: 1000,
    date: {
      formatDefault: "YYYY-MM-DD",
      formatPt: "DD-MM-YYYY",
      formatEs: "YYYY-MM-DD"
    },
    taxRate: 0.23,
    currencyConfig: {
      symbol: "€",
      symbolEncoded: "&euro;",
      thousandsSeparator: ".",
      fractionCount: 2,
      fractionSeparator: ",",
      symbolPosition: "front",
      symbolSpacing: true
    }
  }
};
