/* eslint-disable no-restricted-globals */

// Old versions of IE11 have the `localStorage` methods as strings
// and any attempts to overwrite those functions fail as they are
// cast back into strings. Using these functions ensures no hard-to-debug
// errors occur.
//
// https://stackoverflow.com/questions/21155137/javascript-localstorage-object-broken-in-ie11-on-windows-7
export const setItem = (item, value) => {
  if (typeof localStorage.setItem !== "function") {
    return Storage.prototype.setItem.call(localStorage, item, value);
  }

  localStorage.setItem(item, value);
};

export const getItem = item => {
  if (typeof localStorage.getItem !== "function") {
    return Storage.prototype.getItem.call(localStorage, item);
  }

  return localStorage.getItem(item);
};

export const removeItem = item => {
  if (typeof localStorage.removeItem !== "function") {
    return Storage.prototype.removeItem.call(localStorage, item);
  }

  return localStorage.removeItem(item);
};
