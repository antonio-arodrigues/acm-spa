import { setItem, removeItem } from "@/utils/localStorage";

export const setAccountData = data => {
  setItem("acmAccount", JSON.stringify(data.user || data));
  setItem("token", data.token);
};

export const unsetAccountData = () => {
  removeItem("acmAccount");
  removeItem("token");
};
