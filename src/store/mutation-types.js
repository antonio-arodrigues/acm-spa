// mutation types
// App Settings
export const SET_LOCALE = "SET_LOCALE";
// Global messages
export const DISPLAY_MESSAGE = "DISPLAY_MESSAGE";
export const CLEAR_MESSAGE = "CLEAR_MESSAGE";
// Site search
export const SET_SEARCH_KEYWORD = "SET_SEARCH_KEYWORD";
export const CLEAR_SEARCH_KEYWORD = "CLEAR_SEARCH_KEYWORD";
export const SITE_SEARCH = "SITE_SEARCH";
// Cart operations
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const INCREMENT_QUANTITY = "INCREMENT_QUANTITY";
export const DECREMENT_QUANTITY = "DECREMENT_QUANTITY";
export const RESET_CART = "RESET_CART";
export const SET_PAYMENT_INSTRUMENT = "SET_PAYMENT_INSTRUMENT";
export const SET_SHIPPING_METHOD = "SET_SHIPPING_METHOD";
export const SET_CLIENT_MESSAGE = "SET_CLIENT_MESSAGE";
// Bike operations
export const SET_MY_BIKE = "SET_MY_BIKE";
export const RESET_MY_BIKE = "RESET_MY_BIKE";
// Pagination operations
export const PAGE_PREVIOUS = "PAGE_PREVIOUS";
export const PAGE_NEXT = "PAGE_NEXT";
export const PAGE_RESET = "PAGE_RESET";
