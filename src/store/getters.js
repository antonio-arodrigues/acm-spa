// state values available to components, retrieve on computed methods
import { VALUES } from "./constants";

export const locale = state => {
  return state.locale || VALUES.DEFAULT_LANG;
};

export const message = state => {
  return state.message;
};

export const page = state => {
  return state.page;
};

export const keyword = state => {
  return state.keyword;
};

export const searchBy = state => {
  return state.searchBy;
};
