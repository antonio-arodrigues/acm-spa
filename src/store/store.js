/* global localStorage */
import Vue from "vue";
import Vuex from "vuex";
import { VALUES } from "./constants";
import * as getters from "./getters";
import * as actions from "./actions";
import mutations from "./mutations";
import Account from "./modules/account";
import Bike from "./modules/bike";
import Cart from "./modules/cart";

Vue.use(Vuex);
Vue.config.devtools = process.env.NODE_ENV === "development";

// app initial state
const state = {
  locale: localStorage.getItem("acmLocale") || VALUES.DEFAULT_LANG,
  loading: false,
  page: 1,
  message: {
    display: false,
    key: null,
    type: null
  },
  keyword: null,
  searchBy: null,
  success: false,
  error: false
};

export const store = new Vuex.Store({
  state,
  getters, // state values available to components, retrieve on computed methods
  mutations, // setters - possible mutations to state
  actions, // actions to dispatch > commit mutation > change the state (use for async ops)
  modules: [Account, Bike, Cart]
});
