import Vue from "vue";
import { getItem, setItem } from "@/utils/localStorage";
import * as types from "../mutation-types";

const defaultBasket = {
  items: [],
  totals: {},
  paymentInstrument: null,
  shippingMethod: null,
  clientMessage: null
};

const roundNumber = amount => Math.round(amount * 100) / 100;
const toCurrency = amount => roundNumber(parseFloat(amount));

const estimateShippingCosts = async itemsQty => {
  return await Vue.axios.get(`/ctt/price/${itemsQty}`);
};

const getUpdatedTotals = async state => {
  const basket = state.basket;
  // const taxRate = Vue.app.config.get("global").taxRate;

  const totalPackageWeight = basket.items.reduce((total, p) => {
    return total + p.quantity * p.weight;
  }, 0);

  const shippingCosts = await estimateShippingCosts(totalPackageWeight);
  const shippingCost =
    !shippingCosts.data || !shippingCosts.data.length
      ? null
      : basket.paymentInstrument === "TRF"
      ? toCurrency(shippingCosts.data[0].T1) // transferencia
      : toCurrency(shippingCosts.data[0].T2); // cobrança

  const total = basket.items.reduce((total, p) => {
    return total + p.price * p.quantity;
  }, 0);

  if (shippingCost) {
    // calculate taxes
    // const taxes = total * taxRate;
    // calculate grand total
    // const grandTotal = toCurrency(total + taxes);
    const grandTotal = toCurrency(total + shippingCost);
    return {
      products: toCurrency(total),
      shippingCost,
      grandTotal
    };
  } else {
    const grandTotal = toCurrency(total);
    return {
      products: toCurrency(total),
      shippingCost,
      grandTotal
    };
  }
};

export default {
  // initial state
  state: {
    basket: JSON.parse(getItem("acmBasket")) || defaultBasket
  },

  getters: {
    basket: state => state.basket,
    allProducts: state => state.basket.items || [], // would need action/mutation if data fetched async
    cartItemsCount: state =>
      state.basket.items ? state.basket.items.length : 0,
    paymentInstrument: state => state.basket.paymentInstrument || null,
    shippingMethod: state => state.basket.shippingMethod || null,
    clientMessage: state => state.basket.clientMessage || null
  },

  mutations: {
    /**
     * Add item to cart
     * Increment quantity if already there
     */
    [types.ADD_TO_CART](state, product) {
      let item;
      const bike = Vue.app.store.getters.bike || { id: 0, label: "" };

      // Check if we already have this product
      if (bike) {
        // ...for current selected bike
        item = state.basket.items.find(
          p => p.id === product.id && p.bike.id === bike.id
        );
      } else {
        // ...no bike selected
        item = state.basket.items.find(p => p.id === product.id);
      }

      if (!item) {
        console.log(">>> product:", product);
        state.basket.items.push({
          ...{ bike },
          ...product,
          price: product.price,
          quantity: 1
        });
      } else {
        item.quantity++;
      }

      // update totals
      getUpdatedTotals(state).then(totals => {
        console.table(totals);
        Object.assign(state.basket, { totals });
        // persist
        setItem("acmBasket", JSON.stringify(state.basket));
      });
    },

    /**
     * Remove item from cart
     */
    [types.REMOVE_FROM_CART](state, product) {
      const index = state.basket.items.findIndex(t => t.id === product.id);
      if (index >= 0) {
        state.basket.items.splice(index, 1);
        // update totals
        getUpdatedTotals(state).then(totals => {
          Object.assign(state.basket, { totals });
          // console.table(totals, state.basket.totals);
          // persist
          setItem("acmBasket", JSON.stringify(state.basket));
        });
      } else {
        console.warn("[REMOVE_FROM_CART] Error removing product:", {
          index,
          product,
          basket: state.basket
        });
      }
    },

    /**
     * Increase item quantity
     */
    [types.INCREMENT_QUANTITY](state, product) {
      const item = state.basket.items.find(p => p.id === product.id);
      if (item) {
        item.quantity++;
        // update totals
        getUpdatedTotals(state).then(totals => {
          console.table(totals);
          Object.assign(state.basket, { totals });
          // persist
          setItem("acmBasket", JSON.stringify(state.basket));
        });
      }
    },

    /**
     * Decrease item quantity (min: 1)
     */
    [types.DECREMENT_QUANTITY](state, product) {
      const item = state.basket.items.find(p => p.id === product.id);
      if (item && item.quantity > 1) {
        item.quantity--;
        // update totals
        getUpdatedTotals(state).then(totals => {
          console.table(totals);
          Object.assign(state.basket, { totals });
          // persist
          setItem("acmBasket", JSON.stringify(state.basket));
        });
      }
    },

    /**
     * Clear all cart items
     */
    [types.RESET_CART](state) {
      state.basket = defaultBasket;
      setItem("acmBasket", JSON.stringify(state.basket));
    },

    /**
     * Set payment option
     */
    async [types.SET_PAYMENT_INSTRUMENT](state, option) {
      Object.assign(state.basket, { paymentInstrument: option });

      // recalculate cart totals with new payment option
      const totals = await getUpdatedTotals(state);
      console.table(totals);
      Object.assign(state.basket, { totals });

      setItem("acmBasket", JSON.stringify(state.basket));
    },

    /**
     * Set shippment option
     */
    [types.SET_SHIPPING_METHOD](state, option) {
      Object.assign(state.basket, { shippingMethod: option });
      setItem("acmBasket", JSON.stringify(state.basket));
    },

    /**
     * Set client message
     */
    [types.SET_CLIENT_MESSAGE](state, option) {
      Object.assign(state.basket, { clientMessage: option });
      setItem("acmBasket", JSON.stringify(state.basket));
    }
  },

  actions: {
    addToCart({ dispatch, commit }, product) {
      commit(types.ADD_TO_CART, product);
      dispatch("displaySuccessMessage", "pages.cart.add.success", {
        root: true
      });
    },

    incrementQuantity({ commit }, product) {
      commit(types.INCREMENT_QUANTITY, product);
    },

    decrementQuantity({ commit }, product) {
      commit(types.DECREMENT_QUANTITY, product);
    },

    removeFromCart({ commit }, product) {
      commit(types.REMOVE_FROM_CART, product);
    },

    resetCart({ commit }) {
      commit(types.RESET_CART);
    },

    setPaymentInstrument({ commit }, option) {
      commit(types.SET_PAYMENT_INSTRUMENT, option);
    },

    setShippingMethod({ commit }, option) {
      commit(types.SET_SHIPPING_METHOD, option);
    },

    setClientMessage({ commit }, option) {
      commit(types.SET_CLIENT_MESSAGE, option);
    }
  }
};
