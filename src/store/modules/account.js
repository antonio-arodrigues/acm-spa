import Vue from "vue";
import { getItem, setItem } from "@/utils/localStorage";
import { setAccountData, unsetAccountData } from "../utils";

const axioHeaders = {
  headers: { "Content-Type": "application/json" }
};

const handleError = (store, context, error) => {
  const message = error
    ? error
    : {
        message: "Server error",
        status: 500,
        t: "error.server"
      };
  console.warn(`[ACCOUNT] [${context}]`, { error: message });
  store.commit("authError", message);
  return error;
};

export default {
  state: {
    account: JSON.parse(getItem("acmAccount")) || {},
    token: getItem("token") || null,
    acmEmail: getItem("acmEmail") || null,
    loggedIn: !!getItem("token") && !!getItem("acmAccount"),
    validationEmailSent: false
  },

  getters: {
    account: state => {
      return state.account || {};
    },

    token: state => {
      return state.token;
    },

    acmEmail: state => {
      return state.acmEmail;
    },

    loggedIn: state => {
      return state.loggedIn;
    },

    auth: state => {
      if (!state.token) {
        return false;
      }
      return true;
    },

    authError: state => {
      return state.authError;
    },

    validationEmailSent: state => {
      return state.validationEmailSent;
    }
  },

  mutations: {
    login: (state, data) => {
      setAccountData(data);
      state.loggedIn = true;
      state.account = data.user;
      state.token = data.token;
      state.acmEmail = data.user.email;
    },

    logout: state => {
      unsetAccountData();
      state.loggedIn = false;
      state.account = {};
      state.token = null;
      state.acmEmail = null;
    },

    ACCOUNT_UPDATED: (state, data) => {
      console.log("ACCOUNT_UPDATED", data);
      state.account = data;
      setAccountData(data);
    },

    ACCOUNT_SET_TOKEN: (state, token) => {
      state.token = token;
    },

    ACCOUNT_SET_EMAIL: (state, email) => {
      state.acmEmail = email;
      setItem("acmEmail", email);
    },

    ACCOUNT_REGISTERED: state => {
      state.validationEmailSent = true;
    },

    authError: (state, authError) => {
      Object.assign(state, { authError });
    }
  },

  actions: {
    loadFromHash: async (store, hash) => {
      const response = await Vue.axios
        .get(`/customer/activation/${hash}`)
        .catch(reason => handleError(store, "Activation/hash (1)", reason));
      return response;
    },

    activate: async (store, payload) => {
      const { hash, body } = payload;
      const response = await Vue.axios
        .patch(`/customer/activate/${hash}`, JSON.stringify(body), axioHeaders)
        .catch(reason => handleError(store, "Activate (1)", reason));
      return response;
    },

    reset: async (store, payload) => {
      const { hash, body } = payload;
      const response = await Vue.axios
        .patch(`/customer/reset/${hash}`, JSON.stringify(body), axioHeaders)
        .catch(reason => handleError(store, "Reset (1)", reason));
      return response;
    },

    login: async (store, account) => {
      const params = {
        email: account.email,
        password: account.password
      };

      const response = await Vue.axios
        .post("/realm/auth", params)
        .catch(reason => handleError(store, "Login (1)", reason));

      if (response) {
        if (response && response.status && parseInt(response.status) === 200) {
          store.commit("login", response.data);
        } else {
          handleError(store, "Login (2)", response.data);
        }
      } else {
        handleError(store, "Login (3)", null);
      }
      return response;
    },

    register: async (store, payload) => {
      const response = await Vue.axios
        .post("/customer", JSON.stringify(payload), axioHeaders)
        .catch(reason => handleError(store, "Register Account (2)", reason));

      if (response.status && parseInt(response.status) === 201) {
        store.commit("ACCOUNT_REGISTERED");
      } else {
        return handleError(store, "Register Account (1)", response.data);
      }
      return response;
    },

    accountUpdate: async (store, payload) => {
      const response = await Vue.axios
        .patch(
          `/realm/customer/${payload.id}`,
          JSON.stringify(payload.form),
          axioHeaders
        )
        .catch(reason => handleError(store, "Update Account (2)", reason));

      if (response.status && parseInt(response.status) === 200) {
        store.commit("ACCOUNT_UPDATED", response.data);
      } else {
        return handleError(store, "Update Account (1)", response);
      }
      return response;
    },

    setToken: (store, token) => {
      store.commit("ACCOUNT_SET_TOKEN", token);
    },

    setEmail: (store, email) => {
      store.commit("ACCOUNT_SET_EMAIL", email);
    },

    logout: store => store.commit("logout")
  }
};
