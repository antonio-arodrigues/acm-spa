import Vue from "vue";
import { getItem, setItem, removeItem } from "@/utils/localStorage";
import * as types from "../mutation-types";

// const axioHeaders = {
//   headers: { "Content-Type": "application/json" }
// };

const handleError = (store, context, error) => {
  const message = error
    ? error
    : {
        message: "Server error",
        status: 500,
        t: "error.server"
      };
  console.warn(`[BIKE] [${context}]`, { error: message });
  return error;
};

export default {
  // initial state
  state: {
    bike: JSON.parse(getItem("acmMyBike")) || null
  },

  getters: {
    bike: state => state.bike,
    myBike(state) {
      return state.bike;
    }
  },

  mutations: {
    /**
     * Set my bike data
     */
    [types.SET_MY_BIKE](state, bike) {
      state.bike = bike;
      setItem("acmMyBike", JSON.stringify(bike));
    },

    /**
     * Clear  my bike data
     */
    [types.RESET_MY_BIKE](state) {
      state.bike = null;
      removeItem("acmMyBike");
    }
  },

  actions: {
    setMyBike({ commit }, bike) {
      commit(types.SET_MY_BIKE, bike);
    },

    resetMyBike({ commit }) {
      commit(types.RESET_MY_BIKE);
    },

    loadBikeYearRange: async (store, bikeId) => {
      const response = await Vue.axios
        .get(`/brand-model/model/${bikeId}/years`)
        .catch(reason => handleError(store, "Bike/load_year_range", reason));
      return response;
    }
  }
};
