import * as types from "./mutation-types";

/**
 * (async) actions to dispatch > commit mutation > change store.state
 */

export const displayDefaultMessage = ({ commit }, key) => {
  commit(types.DISPLAY_MESSAGE, { key, type: "primary" });
};

export const displaySuccessMessage = ({ commit }, key) => {
  commit(types.DISPLAY_MESSAGE, { key, type: "success" });
};

export const displayWarningMessage = ({ commit }, key) => {
  commit(types.DISPLAY_MESSAGE, { key, type: "warn" });
};

export const displayErrorMessage = ({ commit }, key) => {
  commit(types.DISPLAY_MESSAGE, { key, type: "danger" });
};

export const pagePrevious = ({ commit }) => {
  commit(types.PAGE_PREVIOUS);
};

export const pageNext = ({ commit }, maxPages) => {
  commit(types.PAGE_NEXT, { maxPages });
};

export const pageReset = ({ commit }) => {
  commit(types.PAGE_RESET);
};

export const setSearchKeyword = ({ commit }, keyword) => {
  commit(types.SET_SEARCH_KEYWORD, keyword);
};

export const clearSearchKeyword = ({ commit }) => {
  commit(types.CLEAR_SEARCH_KEYWORD);
};

export const siteSearch = ({ commit }) => {
  commit(types.SITE_SEARCH);
};
