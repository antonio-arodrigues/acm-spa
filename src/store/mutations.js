/* global localStorage */
import Vue from "vue";
import * as types from "./mutation-types";

/**
 * mutation setters - possible mutations to state
 */

export default {
  [types.SET_LOCALE](state, payload) {
    state.locale = payload;
    localStorage.setItem("acmLocale", payload);
  },

  [types.DISPLAY_MESSAGE](state, payload) {
    const { key, type } = payload;
    Vue.app.notify(key, type);
  },

  [types.PAGE_PREVIOUS](state) {
    state.page = state.page === 1 ? 1 : state.page - 1;
  },

  [types.PAGE_NEXT](state, payload) {
    state.page = state.page === payload.maxPages ? state.page : state.page + 1;
  },

  [types.PAGE_RESET](state) {
    state.page = 1;
  },

  [types.SET_SEARCH_KEYWORD](state, payload) {
    state.keyword = payload;
  },

  [types.CLEAR_SEARCH_KEYWORD](state) {
    state.keyword = null;
  },

  [types.SITE_SEARCH](state) {
    state.searchBy = state.keyword;
  }
};
