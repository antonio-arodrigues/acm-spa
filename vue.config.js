const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  css: {
    modules: true,
    loaderOptions: {
      sass: {
        data: '@import "@/styles/_globals.scss";'
      }
    }
  },

  configureWebpack: {
    devtool: "source-map",
  },

  chainWebpack(config) {
    config.plugins.delete('prefetch');
    config.plugin('CompressionPlugin').use(CompressionPlugin);
  },

  lintOnSave: false,
  publicPath: "",
  outputDir: undefined,
  assetsDir: "assets",
  runtimeCompiler: undefined,
  productionSourceMap: false,
  parallel: undefined,
  integrity: false,

  devServer: {
    https: false,
    clientLogLevel: "debug"
    // proxy: {
    //   "/api/v1": {
    //     target: "http://localhost:8000",
    //     ws: true,
    //     changeOrigin: true
    //   }
    // }
  }
};
